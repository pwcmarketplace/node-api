var express = require('express');

// Mongoose import
var mongoose = require('mongoose');

// Mongoose connection to MongoDB (ted/ted is readonly)
mongoose.connect('mongodb://mongodbhost:27017/employees', function (error) {
    if (error) {
        console.log(error);
    }
});

// Mongoose Schema definition
var Schema = mongoose.Schema;
var UserSchema = new Schema({
    empID: String,
    name: String,
    address: String,
    phone: String
});

// Mongoose Model definition
var Employee = mongoose.model('users', UserSchema);

// Bootstrap express
var cors = require('cors');
var app = express();
app.use(cors());

// URLS management

app.post('/users/:id', function (req, res) {
    var ID = req.params.id;
    var records = [
        {
            "name" : "James Williams",
            "address": "1 Woolworths Way Bella Vista NSW 2153",
            "phone": "+61 444 555 6278"
        },
        {
            "name" : "George Slattery",
            "address": "A1 Richmond Drive Homebush West NSW 2143",
            "phone": "+61 400 366 2343"
        },
        {
            "name" : "Alex Luan",
            "address": "55 Coonara Ave West Pennant Hills 2150",
            "phone": "+61 454 231 5622"
        }            
    ];
    for (var i=0; i<3; i++) {
        Employee.update({'empID':i+1}, records[i], {upsert:true}, function(update_err) {
            if (update_err) return;
        });
    }

    Employee.findOne({ "empID" : ID }, function (err, docs) {
        res.json(docs);
    });
});

// Start the server
app.listen(80);

